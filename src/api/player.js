import axios from "axios";
import keycloak from "../keycloak";
import { getHeaders } from "../services/HttpService";

const apiUrl = process.env.REACT_APP_API_URL;

export const getPlayers = async () => {
    try {
        const response = await axios.get(`${apiUrl}/players`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No players found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

/**
 * Gets all players in a given game, returns all players if no game is specified
 * @param {*} gameId 
 * @returns Players in given game, otherwise all players
 */
export const getPlayersInGame = async (gameId) => {
    if(gameId === undefined){
        return getPlayers();
    }
    try {
        const response = await axios.get(`${apiUrl}/games/${gameId}/players`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No players found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const addPlayer = async (player) => {
    try {
        if(player === undefined){
            throw new Error("Player body is undefined");
        }
        const response = await axios.post(`${apiUrl}/players/`, player, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const newPlayer = async (id, isHuman, isPatientZero) => {
    try {
        if(id === undefined){
            throw new Error("ID is not defined");
        }
        let newPlayer = null;
        if(isHuman){
            newPlayer = {patientZero: false, human: true}
        }
        else{
            newPlayer = {patientZero: isPatientZero, human: isHuman}
        }
        const response = await axios.post(`${apiUrl}/games/${id}/players/`, newPlayer, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const getCurrentPlayer = async (id) => {
    try {
        const response = await axios.get(`${apiUrl}/games/${id}/players/current`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No players found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const deletePlayer = async (id) => {
    try {
        if(id === undefined){
            throw new Error("ID is undefined");
        }
        const response = await axios.delete(`${apiUrl}/players/${id}`, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const updatePlayer = async (id, player) => {
    try {
        if(id === undefined){
            throw new Error("ID undefined");
        }
        if(player === undefined){
            throw new Error("Player undefined");
        }
        const response = await axios.put(`${apiUrl}/players/${id}`, player, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}