import axios from "axios";
import { getHeaders } from "../services/HttpService";

const apiUrl = process.env.REACT_APP_API_URL;

export const getUser = async () => {
    try {
        const response = await axios.get(`${apiUrl}/users/current`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No users found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const registerUser = async () => {
    try {
        const response = await axios.post(`${apiUrl}/users/register`, null, getHeaders());
        return [null, response]
    }
    catch (error) {
        return [error.message, []]
    }
}