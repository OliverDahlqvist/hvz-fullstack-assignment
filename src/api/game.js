import axios from "axios";
import { getHeaders } from "../services/HttpService";

const apiUrl = process.env.REACT_APP_API_URL;

export const getGames = async () => {
    try {
        const response = await axios.get(`${apiUrl}/games`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No games found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const getGame = async (id) => {
    try {
        const response = await axios.get(`${apiUrl}/games/${id}`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No games found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const addGame = async (game) => {
    try {
        if(game === undefined){
            throw new Error("Mission body is not defined");
        }
        const response = await axios.post(`${apiUrl}/games/`, game, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const deleteGame = async (id) => {
    try {
        if(id === undefined){
            throw new Error("ID is not defined");
        }
        const response = await axios.delete(`${apiUrl}/games/${id}`, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const updateGame = async (id, game) => {
    try {
        if(id === undefined){
            throw new Error("ID undefined");
        }
        if(game === undefined){
            throw new Error("Game undefined");
        }
        const response = await axios.put(`${apiUrl}/games/${id}`, game, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}