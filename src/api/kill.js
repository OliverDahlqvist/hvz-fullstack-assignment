import axios from "axios";
import { getHeaders } from "../services/HttpService";

const apiUrl = process.env.REACT_APP_API_URL;

export const addKill = async (kill, gameId) => {
    try {
        if(kill === undefined){
            throw new Error("Mission body is not defined");
        }
        const response = await axios.post(`${apiUrl}/games/${gameId}/kills/`, kill, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}