import axios from "axios";
import { getHeaders } from "../services/HttpService";

const apiUrl = process.env.REACT_APP_API_URL;

export const getMissions = async (gameId) => {
    try {
        if(gameId === undefined){
            throw new Error("Game ID is undefined");
        }
        const response = await axios.get(`${apiUrl}/games/${gameId}/missions`, getHeaders());
        if (response.data === undefined) {
            throw new Error("No missions found");
        }
        return [null, response.data]
    }
    catch (error) {
        return [error.message, []]
    }
}

export const addMission = async (gameId, mission) => {
    try {
        if(gameId === undefined){
            throw new Error("Game id is undefined");
        }
        if(mission === undefined){
            throw new Error("Mission body is undefined");
        }
        const response = await axios.post(`${apiUrl}/games/${gameId}/missions`, mission, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const deleteMission = async (gameId, missionId) => {
    try {
        if(gameId === undefined){
            throw new Error("Game ID is undefined");
        }
        if(missionId === undefined){
            throw new Error("Mission ID is undefined");
        }
        const response = await axios.delete(`${apiUrl}/games/${gameId}/missions/${missionId}`, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}

export const updateMission = async (gameId, missionId, mission) => {
    try {
        if(gameId === undefined){
            throw new Error("Game ID is undefined");
        }
        if(missionId === undefined){
            throw new Error("Mission ID is undefined");
        }
        const response = await axios.put(`${apiUrl}/games/${gameId}/missions/${missionId}`, mission, getHeaders());
        return [null, response.data]
    }
    catch(error){
        return [error.message, []]
    }
}