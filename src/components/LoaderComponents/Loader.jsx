import { BarLoader, ScaleLoader } from "react-spinners";
import "./Loader.css";

/**
 * Loader which can be used throught the application for consistency.
 * @returns Loader component.
 */
export default function Loader({style = "Bar", margin, visibility = true, position = "relative", width = 100 }) {
    const getLoader = () => {
        if(style === "Scale"){
            return(<ScaleLoader width={width} color="#838383" />)
        }
        else
        return(<BarLoader width={width} color="#838383" />)
    }
    return (
            <div className="loader" style={{ margin: { margin }, visibility: visibility ? "visible" : "hidden", position: position}}>
                {getLoader()}
            </div>);
}