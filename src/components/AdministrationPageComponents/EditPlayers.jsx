import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import "../AdministrationPageComponents/Admin.css";
import { getPlayers, getPlayersInGame } from '../../api/player';
import { Card, Table } from 'react-bootstrap';
import PlayerItem from './PlayerItem';
import { getGames } from '../../api/game';
import { ScaleLoader } from 'react-spinners';
import Loader from '../LoaderComponents/Loader';


const EditPlayers = (props) => {
    const [playersLoaded, setPlayersLoaded] = useState(false);
    const [gamesLoaded, setGamesLoaded] = useState(false);
    const [players, setPlayers] = useState([]);
    const [games, setGames] = useState([]);
    const [error, setError] = useState(false);


    const fetchPlayers = async () => {
        const [error, result] = await getPlayersInGame();
        if (!error) {
            setPlayers(result);
            setPlayersLoaded(true);
        }
        else {
            setError(error);
        }
    }

    const fetchGames = async () => {
        const [error, result] = await getGames();
        if (!error) {
            setGames(result);
            setGamesLoaded(true);
        }
        else {
            setError(error);
        }
    }

    useEffect(() => {
        fetchPlayers();
        fetchGames();
    }, []);

    const refreshData = async () => {
        fetchPlayers();
        fetchGames();
    }

    if(!playersLoaded || !gamesLoaded){
        return(<Loader></Loader>)
    }
    return (
        <Card style={{padding: "2rem", borderRadius: "3rem 3rem"}}>
            <Table striped bordered hover size='sm'  style={{margin: "0px"}}>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Game</th>
                        <th>Faction</th>
                        <th>Patient Zero</th>
                        <th>Save</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {players.map((player) => {
                        return(<PlayerItem key={player.id} player={player} games={games} refreshPlayers={refreshData}></PlayerItem>);
                    })}
                </tbody>
            </Table>
        </Card>
    );
}

export default EditPlayers;