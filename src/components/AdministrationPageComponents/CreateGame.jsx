import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import "../AdministrationPageComponents/Admin.css";
import { Button, Col, Container, Form, Row, ToastContainer } from 'react-bootstrap';
import Map from '../GamePageComponents/Map';
import { Rectangle, Pane } from "react-leaflet";
import { addGame } from '../../api/game';
import { convertToId } from '../../util/GameStateConverter';
import Alert from '../Alerts/Alert';
import AlertContainer from '../Alerts/AlertContainer';

const gameNameConfig = {
    required: true,
    minLength: 4
}

let coordinatesNW = { lat: 0, lng: 0 };
let coordinatesSE = { lat: 0, lng: 0 };

const CreateGame = (props) => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()

    const [isLoaded, setIsLoaded] = useState(false);
    const [games, setGames] = useState([]);
    const [error, setError] = useState(false);
    const [alerts, setAlerts] = useState([]);

    const [rectangle, setNewRectangle] = useState(null);

    const onClickFunction = (arg) => {
        coordinatesSE = coordinatesNW;
        coordinatesNW = arg;
        let bounds = { coordinatesNW: coordinatesNW, coordinatesSE: coordinatesSE };
        setNewRectangle(DisplaySelectionRectangle(bounds));
    }

    const DisplaySelectionRectangle = (coords) => {
        if (coords.coordinatesNW !== undefined && coords.coordinatesSE !== undefined)
            return (
                <Pane>
                    <Rectangle key={10} bounds={[
                        [coords.coordinatesNW.lat, coords.coordinatesNW.lng],
                        [coords.coordinatesSE.lat, coords.coordinatesSE.lng],
                    ]} color="orange">
                    </Rectangle>
                </Pane>
            )
        else return null
    }

    const onSubmit = ({ name, description, gameState }) => {
        const gameStateValue = convertToId(gameState);

        const gameObject = {
            gameName: name,
            description: description,
            gameState: gameStateValue,
            nwLat: coordinatesNW.lat,
            nwLng: coordinatesNW.lng,
            seLat: coordinatesSE.lat,
            seLng: coordinatesSE.lng,
            missions: [],
            players: []
        };

        addGame(gameObject)
        const activeAlerts = alerts;
        activeAlerts.push({ text: `Created new game (${gameObject.gameName})` });
        setAlerts(activeAlerts);
    };

    return (
        <div>
            <AlertContainer>
                {alerts && alerts.map((alert) => {
                    console.log(alert);
                    return (<Alert text={alert.text} title={alert.title}></Alert>)
                })}
            </AlertContainer>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Container>
                    <Row>
                        <Col>
                            <label htmlFor='inputGameName'>Name</label>
                            <input {...register("name", gameNameConfig)} className="form-control" aria-describedby="enterGameName" placeholder="Enter name of game" />
                        </Col>
                        <Col>
                            <label>Game state</label>
                            <Form.Select {...register("gameState")}>
                                <option>Registration</option>
                                <option>In progress</option>
                                <option>Complete</option>
                            </Form.Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <label htmlFor='inputGameName'>Description</label>
                            <textarea {...register("description")} rows="1" className="form-control" aria-describedby="enterGameName" placeholder="Enter a game description (optional)" />
                        </Col>
                        <Col>
                            <div style={{
                                display: 'flex',
                                marginTop: '18px',
                                justifyContent: 'space-evenly',
                            }}>
                                <p>NW: {coordinatesNW.lat}, {coordinatesNW.lng}</p>
                                <p>SE: {coordinatesSE.lat}, {coordinatesSE.lng}</p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button variant='outline-dark' type="submit" className="btn">Submit</Button>
                        </Col>
                    </Row>
                </Container>
            </form>
            <div>
                <Map updateCoordinates={onClickFunction} rectangle={rectangle}></Map>
            </div>
        </div>
    );
}

export default CreateGame;