import React, { useEffect, useState } from 'react';
import "../AdministrationPageComponents/Admin.css";
import { Button, Card, Row, Tab, Tabs, Nav, Col } from 'react-bootstrap';
import CreateGame from './CreateGame';
import EditGame from './EditGame';
import EditPlayers from './EditPlayers';
import KeycloakRoute from '../../routes/KeycloakRoute';

const BUTTON_STYLES = {
    width: '150px',
    height: '50px',
    marginRight: '10px',
    padding: '3px'
}

const Admin = () => {
    const [visibleComponent, setVisibleComponent] = useState(null);

    // Used to handle all the tab items, workaround since tabs didn't work well with leaflet maps
    function handleClick(component) {
        switch (component) {
            case "CreateGame":
                setVisibleComponent(<CreateGame />)
                break;
            case "EditGame":
                setVisibleComponent(<EditGame />)
                break;
            case "EditPlayers":
                setVisibleComponent(<EditPlayers />)
                break;
            default:
                break;
        }
    }

    return (
        <KeycloakRoute role='Admin'>
            <div className='center-tabs'>
                <Tabs defaultActiveKey="profile" id="tabs" className="mb-3" onSelect={(k) => handleClick(k)}>
                    <Tab eventKey="CreateGame" title='Create Game' />
                    <Tab eventKey='EditGame' title='Edit Game' />
                    <Tab eventKey='EditPlayers' title='Edit Players' />
                </Tabs>
            </div>
            {visibleComponent}
        </KeycloakRoute>
    );
}



export default Admin;