import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import "../AdministrationPageComponents/Admin.css";
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import Map from '../GamePageComponents/Map';
import { Rectangle, Pane } from "react-leaflet";
import Loader from '../LoaderComponents/Loader';
import { getGames, deleteGame, updateGame } from '../../api/game';
import { addMission } from '../../api/mission';
import { convertToId } from '../../util/GameStateConverter';
import { PuffLoader } from 'react-spinners';

const gameNameConfig = {
    required: true,
    minLength: 4
}

let coordinatesNW = { lat: 0, lng: 0 };
let coordinatesSE = { lat: 0, lng: 0 };

const EditGame = (props) => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()

    const [isLoaded, setIsLoaded] = useState(false);
    const [games, setGames] = useState(null);
    const [error, setError] = useState(false);
    const [selectedId, setSelectedId] = useState(0);
    const [center, setCenter] = useState(null);
    const [loading, setLoading] = useState(false);
    const [reloading, setReloading] = useState(false);

    // Used to update missions in map child.
    const [missionRefresher, setMissionRefresher] = useState(false);
    const [gamesRefresher, setGamesRefresher] = useState(false);
    // Switches boolean to update child effect hook
    function refreshMissions() {
        setMissionRefresher(!missionRefresher);
    }
    function refreshGames() {
        setGamesRefresher(!gamesRefresher);
    }

    const [rectangle, setNewRectangle] = useState(null);

    const onClickFunction = (arg) => {
        coordinatesSE = coordinatesNW;
        coordinatesNW = arg;
        let bounds = { coordinatesNW: coordinatesNW, coordinatesSE: coordinatesSE };
        setNewRectangle(displaySelectionRectangle(bounds));
    }

    const fetchGames = async () => {
        
        const [error, result] = await getGames();
        setIsLoaded(true);
        setReloading(false);
        setGames(result);
        if (result.length > 0)
            setSelectedId(result[0].id);
    }

    const reloadGames = async () => {
        setReloading(true);
        fetchGames();
    }

    useEffect(() => {
        setIsLoaded(false);
        fetchGames()
    }, [gamesRefresher]);

    const displaySelectionRectangle = (coords) => {
        if (coords.coordinatesNW !== undefined && coords.coordinatesSE !== undefined)
            return (
                <Pane>
                    <Rectangle key={10} bounds={[
                        [coords.coordinatesNW.lat, coords.coordinatesNW.lng],
                        [coords.coordinatesSE.lat, coords.coordinatesSE.lng],
                    ]} color="orange">
                    </Rectangle>
                </Pane>
            )
        else return null
    }

    const getCurrentCenter = (coords) => {
        setCenter(coords);
    }

    const addNewMission = () => {
        const newMission = { name: "(Rename me)", lat: center.X, lng: center.Y, visibleByHuman: false, visibleByZombie: false };
        setLoading(true);
        addMission(selectedId, newMission)
            .then(() => {
                refreshMissions();
                setLoading(false);
            });
    }

    const onDeleteGame = () => {
        setLoading(true);
        deleteGame(selectedId)
            .then(() => {
                refreshGames();
                setLoading(false);
            });
    }

    const onSubmit = ({ name, description, gameState }) => {
        //convertToId(gameState);
        const gameObject = {
            id: selectedId,
            gameName: name,
            description: description,
            gameState: 0,
            nwLat: coordinatesNW.lat,
            nwLng: coordinatesNW.lng,
            seLat: coordinatesSE.lat,
            seLng: coordinatesSE.lng,
            missions: [],
            players: []
        };
        updateGame(selectedId, gameObject)
            .then(() => {
                fetchGames();
            })
    };

    if (!isLoaded)
        return <Loader visibility={true} />
    else if (isLoaded && games.length === 0)
        return (
            <div style={{ justifyContent: "center", display: "flex" }}>
                <Container>
                    <Row className="justify-content-md-center">
                        <Col md="auto">No games found</Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col md="auto">
                            <Button variant="outline-dark" onClick={() => reloadGames()}>
                                Reload games
                            </Button>
                        </Col>
                    </Row>
                    {reloading && <Row className="justify-content-md-center">
                        <Col md="auto">
                            <PuffLoader color="#36d7b7" />
                        </Col>
                    </Row>}
                </Container>
            </div>
        )
    else
        return (
            <div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Container>
                        <Row>
                            <Col>
                                <label>Games</label>
                                <Form.Select value={selectedId} onChange={(e) => setSelectedId(e.currentTarget.value)}>
                                    {games && games.map((game) => (
                                        <option key={game.id} value={game.id}>{game.gameName}</option>
                                    ))}
                                </Form.Select>
                            </Col>
                            <Col>
                                <label>New Name</label>
                                <input {...register("name", gameNameConfig)} rows="1" className="form-control" aria-describedby="enterGameName" placeholder="Enter a new name" />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <label>Description</label>
                                <textarea {...register("description")} rows="1" className="form-control" aria-describedby="enterGameName" placeholder="Enter a game description (optional)" />
                            </Col>
                            <Col>
                                <label>Game state</label>
                                <Form.Select {...register("gameState")}>
                                    <option>Registration</option>
                                    <option>In progress</option>
                                    <option>Complete</option>
                                </Form.Select>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <div>
                                    <Button variant='outline-dark' className="btn" onClick={addNewMission}>
                                        <div style={{ justifyContent: "center", display: "flex" }}>
                                            <Loader visibility={loading} width={140} position={"absolute"} />
                                        </div>
                                        <span> Add new mission</span>
                                    </Button>
                                    <Button variant='outline-success' style={{ marginLeft: "5px", marginRight: "5px" }} type="submit" className="btn">Submit</Button>
                                    <Button variant='outline-danger' className="btn" onClick={onDeleteGame}>Delete</Button>
                                </div>
                            </Col>
                            <Col>
                                <div style={{
                                    display: 'flex',
                                    marginTop: '18px',
                                    justifyContent: 'space-evenly',
                                }}>
                                    <p>NW: {coordinatesNW.lat}, {coordinatesNW.lng}</p>
                                    <p>SE: {coordinatesSE.lat}, {coordinatesSE.lng}</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </form>
                <div className='map-wrapper'>
                    <Map updateCoordinates={onClickFunction} rectangle={rectangle} gameId={selectedId} currentCenter={getCurrentCenter} refreshMissions={missionRefresher}></Map>
                </div>
            </div>
        );
}

export default EditGame;