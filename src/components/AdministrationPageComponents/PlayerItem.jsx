import React, { useEffect, useState } from 'react';
import "../AdministrationPageComponents/Admin.css";
import { ToggleButton, Button, Form } from 'react-bootstrap';
import { deletePlayer, updatePlayer } from '../../api/player';

export const PlayerItem = ({ player, games, refreshPlayers }) => {
    const [humanChecked, setHumanChecked] = useState(player.human);
    const [patientZeroChecked, setPatientZeroChecked] = useState(player.patientZero);
    const [selectedId, setSelectedId] = useState(player.game);
    const [hasChanged, setHasChanged] = useState(false);

    useEffect(() => {
        setHasChanged(getChanged);
    }, [player.human, player.patientZero, player.game])

    const originalPlayer = player;

    const handleIsHuman = (checked) => {
        player.human = checked
        setHumanChecked(player.human);
    }

    const handleIsPatientZero = (checked) => {
        player.patientZero = checked
        setPatientZeroChecked(player.patientZero);
    }

    const onGameChange = (id) => {
        player.game = id;
        setSelectedId(id);
    }

    const handleSave = () => {
        updatePlayer(player.id, player)
    }

    const handleDelete = () => {
        deletePlayer(player.id).then(() => {
            refreshPlayers();
        })
    }

    const getChanged = () => {
        console.log(originalPlayer !== player);
        return originalPlayer !== player;
    }

    const humanId = player.id + "h";
    const patientZeroId = player.id + "z";

    return (
        <tr>
            <td>{player.id}</td>
            <td>
                <Form.Select value={selectedId} onChange={(e) => onGameChange(e.currentTarget.value)}>
                    {games.map((game) => (
                        <option key={game.id} value={game.id}>{game.gameName}</option>
                    ))}
                </Form.Select>
            </td>
            <td>
                <ToggleButton
                    size='sm'
                    id={humanId}
                    type="checkbox"
                    variant="outline-secondary"
                    checked={humanChecked}
                    onChange={(e) => handleIsHuman(e.currentTarget.checked)}>
                    {player.human ? "Human" : "Zombie"}
                </ToggleButton>
            </td>
            <td>
                <ToggleButton
                    size='sm'
                    id={patientZeroId}
                    type="checkbox"
                    variant="outline-secondary"
                    checked={patientZeroChecked}
                    onChange={(e) => handleIsPatientZero(e.currentTarget.checked)}>
                    {player.patientZero ? "Yes" : "No"}
                </ToggleButton>
            </td>
            <td><Button disabled={hasChanged} size='sm' id={player.id} variant="outline-success" onClick={handleSave}>Save</Button></td>
            <td><Button disabled={hasChanged} size='sm' id={player.id} variant="outline-danger" onClick={handleDelete}>Delete</Button></td>
        </tr>)
}

export default PlayerItem;