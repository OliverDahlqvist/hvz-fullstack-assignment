import io from "socket.io-client";
import "../GamePageComponents/GamePageStyle.css";
import { useState, useEffect, useRef } from "react";
import { BsChevronDown } from "react-icons/bs";
import Collapsible from "react-collapsible";

const Chat = () => {
  const [socket, setSocket] = useState();
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    var chatBox = document.getElementById("chatBox");
    chatBox.scrollTop = chatBox.scrollHeight;
  };

  const [messages, setMessages] = useState([]);
  const [currentMessage, setCurrentMessage] = useState("");

  const handleChange = (evt) => {
    evt.preventDefault();
    setCurrentMessage(evt.target.value);
  };

  const sendChat = (evt) => {
    if (document.getElementById("inputField").value === "") {
      //empty input should NOT send message
    } else {
      evt.preventDefault();
      socket.emit("message", currentMessage);
      document.getElementById("inputField").value = "";
    }
  };

  useEffect(() => {
    if (!socket) {
      setSocket(io("//hvz-chat-server.herokuapp.com"));
    }
    if (socket) {
      socket.on("connect", () => {
        socket.emit("joined", { serverchannel: 120 });
        console.log("Connected");
      });
      socket.on("message", (data) => {
        setMessages((messages) => [...messages, data]);
        scrollToBottom();
      });
    }
  }, [socket]);

  const listMessages = messages.map((message, i) => (
    <p key={"messaged" + i}>message: {message} </p>
  ));

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <>
      <Collapsible
        className="chatBox"
        trigger={["Enter Chat", <BsChevronDown />]}
      >
        <div>
          <form onSubmit={handleSubmit}>
            <section className="big-container">
              <header className="msger-header">
                <div className="msger-header-title">
                  <i className="fas fa-ellipsis-h"></i> ChatBox
                </div>
              </header>

              <div>
                <div className="container">
                  <div id="chatBox" className="chatContainer">
                    {listMessages}
                    <div ref={messagesEndRef} />
                  </div>

                  <div className="input">
                    <input
                      id="inputField"
                      onChange={handleChange}
                      type="text"
                      className="inputField"
                      placeholder="Type your message.."
                    ></input>

                    <button
                      type="submit"
                      onClick={sendChat}
                      className="sendBtn"
                    >
                      Send
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </form>
        </div>
      </Collapsible>
    </>
  );
};

export default Chat;
