import React, { useState } from "react";
import { useForm } from "react-hook-form";
import "./GamePageStyle.css";
import { addKill } from "../../api/kill";

export default function BiteCode() {
    // Sets error states and get gameId from localStorage.
  const [error, setError] = useState(null);
  const gameId = localStorage.getItem("gameId");

  // Handle form
  const { register, handleSubmit } = useForm();

  // Method for onSubmit on form
  const onSubmit = async ({ biteCode, story, lat, lng }) => {
    const killObject = {
      biteCode: biteCode,
      story: story,
      lat: parseFloat(lat),
      lng: parseFloat(lng),
    };

    if (biteCode) {
      // Post request to API.
      console.log(gameId);
      console.log(killObject);

      addKill(killObject, gameId);
      alert("The human was turned into a zombie");
    } else {
      return <div>Error: {error.message}</div>;
    }
  };

  return (
    <div className="bite-code">
      <h4 id="header-bite-code">Make Human into Zombie</h4>
      <form onSubmit={handleSubmit(onSubmit)} className="bite-code-form">
        <label htmlFor="biteCode">Insert your Bite Code:</label>
        <input
          {...register("biteCode")}
          type="text"
          className="form-control"
          aria-describedby="submitKill"
          placeholder="Bite Code"
        />

        <label htmlFor="story">Describe the story of the kill:</label>
        <textarea
          {...register("story")}
          type="text"
          className="form-control"
          aria-describedby="submitKill"
          placeholder="Story"
        />

        <label htmlFor="story">GPS coordinates:</label>
        <input
          {...register("lat")}
          type="text"
          className="form-control"
          aria-describedby="submitKill"
          placeholder="Latitude"
        />

        <input
          {...register("lng")}
          type="text"
          className="form-control"
          aria-describedby="submitKill"
          placeholder="Longitude"
        />

        <button type="submit" className="btn btn-warning btn-rounded">
          Sumbit Kill
        </button>
      </form>
    </div>
  );
}
