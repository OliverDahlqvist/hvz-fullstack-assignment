import { Marker, Popup } from "react-leaflet";
import { useState, useRef, useMemo, useCallback, useEffect } from "react";
import { Button, Form, InputGroup, Row, ToggleButton } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { deleteMission, updateMission } from "../../api/mission";
import RenderOnRole from "../../routes/RenderOnRole";
import keycloak from "../../keycloak";

export default function GameMaker({ mission, missionIcon, gameId, missionChanged }) {
    const {
        handleSubmit,
        formState: { errors }
    } = useForm()

    // Deletes the marker.
    const onDelete = () => {
        deleteMission(gameId, mission.id)
            .then(() => {
                missionChanged();
            })
    }

    // Saves the marker to the db.
    const saveMarker = () => {
        updateVariables();
        updateMission(gameId, mission.id, mission)
            .then(() => {
                missionChanged();
            })
    }

    const onSubmit = () => {
        saveMarker();
    }

    const handleChange = (event) => {
        setName(event.target.value);
    }

    function updateVariables() {
        mission.name = name;
        mission.visibleByHuman = visibleByHuman;
        mission.visibleByZombie = visibleByZombie;
    }

    const [draggable, setDraggable] = useState(false)
    const [position, setPosition] = useState([mission.lat, mission.lng])
    const [name, setName] = useState(mission.name);
    const [disabled, setDisabled] = useState(true);
    const [visibleByHuman, setVisibleByHuman] = useState(mission.visibleByHuman);
    const [visibleByZombie, setVisibleByZombie] = useState(mission.visibleByZombie);

    useEffect(() => {
        setDisabled(name === mission.name)
    }, [name, mission.name])

    const markerRef = useRef(null)
    const eventHandlers = useMemo(
        () => ({
            dragend() {
                const marker = markerRef.current
                if (marker != null) {
                    const latLng = marker.getLatLng();
                    setPosition(latLng)
                    mission.lat = latLng.lat;
                    mission.lng = latLng.lng;
                    if (updateMission === undefined) return;
                    updateMission(mission);
                    saveMarker();
                }
            },
        }),
        [],
    )
    const toggleDraggable = useCallback(() => {
        setDraggable((d) => !d)
    }, [])

    const getTitle = () => {
        if (!keycloak.hasRealmRole('Admin')) {
            return (<p>{name}</p>)
        }
        return null;
    }

    return (
        <Marker
            position={position}
            icon={missionIcon}
            draggable={draggable}
            ref={markerRef}
            eventHandlers={eventHandlers}
        >
            <Popup>
                <Row>
                    {/* <p>{mission.name}</p> */}
                    {getTitle()}
                    <RenderOnRole role={'Admin'}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <InputGroup size="sm">
                                <Form.Control
                                    placeholder=""
                                    value={name}
                                    onChange={event => handleChange(event)}
                                />
                                <span>
                                    <Button type="submit" size="sm" variant="outline-success" style={{ borderTopLeftRadius: "0", borderBottomLeftRadius: "0" }}>
                                        Save
                                    </Button>
                                </span>
                            </InputGroup>
                        </form>
                    </RenderOnRole>
                </Row>
                <RenderOnRole role={'Admin'}>
                    <Row>
                        <div style={{ justifyContent: "center", display: "flex" }}>
                            <Button variant="outline-danger" className="btn-sm" onClick={onDelete}>Delete</Button>
                            <ToggleButton variant="outline-dark" size="sm" type="checkbox" checked={visibleByHuman} onClick={() => { setVisibleByHuman(!visibleByHuman) }} style={{ marginLeft: "3px" }}>Human</ToggleButton>
                            <ToggleButton variant="outline-dark" size="sm" type="checkbox" checked={visibleByZombie} onClick={() => { setVisibleByZombie(!visibleByZombie) }} style={{ marginLeft: "3px", marginRight: "3px" }}>Zombie</ToggleButton>
                            <ToggleButton variant="outline-dark" size="sm" type="checkbox" checked={draggable} onClick={toggleDraggable}>{draggable ? "Draggable" : "Move"}</ToggleButton>
                        </div>
                    </Row>
                </RenderOnRole>
            </Popup>
        </Marker>
    );
}