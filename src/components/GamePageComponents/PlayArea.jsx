import { Rectangle, Pane, Tooltip, useMap } from "react-leaflet";
import { useEffect } from "react";

export default function PlayArea(props) {
    const map = useMap();
    const game = props.gameObject.game;
    const center = props.gameObject.center;
    
    // Checks if the props field is changed and flies to the specified coordinates.
    useEffect(() => {
        map.flyTo([center.X, center.Y], 10);
    }, [props.gameObject.game])

    return (
        <div>
            <Rectangle
                key={game.id}
                bounds={[
                    [game.nwLat, game.nwLng],
                    [game.seLat, game.seLng],
                ]}>
                <Pane name={game.gameName} style={{ zIndex: 402 }}>
                    <Tooltip sticky>{game.gameName}</Tooltip>
                </Pane>
            </Rectangle>
        </div>
    );
}