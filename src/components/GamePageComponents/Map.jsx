import "../GamePageComponents/GamePageStyle.css";
import {
  MapContainer,
  TileLayer,
  Pane,
  useMapEvents,
} from "react-leaflet";
import { Icon } from "leaflet";
import missionImg from "../../images/mission.png";
import gravestoneImg from "../../images/gravestone.png";
import React, { useState, useEffect } from "react";
import "./GamePageStyle.css";
import Loader from "../LoaderComponents/Loader";
import PlayArea from "./PlayArea";
import GameMarker from "./GameMarker";
import { getGame } from "../../api/game";
import { getMissions } from "../../api/mission";

export default function Map({ updateCoordinates, rectangle, gameId, currentCenter, refreshMissions, refreshGames }) {
  // Sets error, loading, game, missions and center states.
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isLoadedMissions, setIsLoadedMissions] = useState(false);
  const [game, setGame] = useState(null);
  const [missions, setMissions] = useState([]);
  const [center, setCenter] = useState({ X: 57.70706324660165, Y: 11.965098381042482 });

  // Adds images for gravestones and missions.
  const gravestoneIcon = new Icon({
    iconUrl: gravestoneImg,
    iconSize: [70, 77],
  });

  const missionIcon = new Icon({
    iconUrl: missionImg,
    iconSize: [95, 70],
  });

  // Calculates center on map.
  function calculateCenter(game) {
    const newCenter = { X: (game.nwLat + game.seLat) / 2, Y: (game.nwLng + game.seLng) / 2 };
    setCenter(newCenter);
    if(currentCenter !== undefined)
    currentCenter(newCenter);
  }
  
 // Get game request from API.
  const fetchGame = async () => {
    if (gameId === undefined) {
      setIsLoaded(true);
      return;
    }
    const [error, result] = await getGame(gameId);
    calculateCenter(result);
    setIsLoaded(true);
    setGame(result);
  };

  useEffect(() => {
    fetchGame();
  }, [gameId]);


  // Get mission request from API.
  const fetchMissions = async () => {
    if (gameId === undefined) {
      setIsLoadedMissions(true);
      return;
    }
    const [error, result] = await getMissions(gameId);
    setIsLoadedMissions(true);
    setMissions(result);
  }

  useEffect(() => {
    fetchMissions();
  }, [gameId, refreshMissions]);

  useEffect(() => {
    fetchMissions();
  }, [refreshMissions]);



  // Used to get coordinates.
  const MapEvents = () => {
    useMapEvents({
      click(e) {
        if (updateCoordinates === undefined) return;
        updateCoordinates({ lat: e.latlng.lat, lng: e.latlng.lng });
      },
    });
    return false;
  }

  // Gets missions markers.
  const getMissionMarker = (mission) => {
    if (mission.visibleByHuman && mission.visibleByZombie) {
      return (
        <GameMarker mission={mission} missionIcon={gravestoneIcon} gameId={gameId} missionChanged={updateMission}></GameMarker>
      );
    } else {
      return (
      <GameMarker mission={mission} missionIcon={missionIcon} gameId={gameId} missionChanged={updateMission}></GameMarker>
      );
    }
  }

  const updateMission = (updatedMission) => {
    getMissions();
  }

  // Error and loading handlings.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded || !isLoadedMissions) {
    return <Loader />;
  } else {
    return (
      <MapContainer
        center={[center.X, center.Y]}
        zoom={12}
        scrollWheelZoom={true}
        className="map"
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Pane name="rectanglePane" style={{ zIndex: 401 }}>
          {game !== null && <PlayArea gameObject={{ game, center }} />}
          {rectangle}
        </Pane>

        {missions.map((mission) => (
          <li key={mission.id}>
            <p>{mission.name}</p>
            {getMissionMarker(mission)}
          </li>
        ))}
        <MapEvents></MapEvents>
      </MapContainer>

    );
  }
}
