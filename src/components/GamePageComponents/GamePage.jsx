import "../GamePageComponents/GamePageStyle.css";
import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";
import "./GamePageStyle.css";
import { newPlayer } from "../../api/player";
import { getGame } from "../../api/game";

export default function GamePage() {
  // Sets error, loading and games states.
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [game, setGame] = useState([]);

  // Gets gameId, gameName and description from localStorage.
  const gameId = localStorage.getItem("gameId");
  const gameName = localStorage.getItem("gameName");
  const description = localStorage.getItem("description");


   // Get request from API for game.
  const fetchGame = async () => {
    const [error, result] = await getGame(gameId);
    setIsLoaded(true);
    setGame(result);
  };

  useEffect(() => {
    setIsLoaded(false);
    fetchGame();
  }, [gameId]);


  // OnClick handling for human.
  const chooseHuman = async () => {
    // Post request to API.
    newPlayer(gameId, true, false)
  };

  // OnClick handling for zombie.
  const chooseZombie = async () => {
      // Post request to API.
    newPlayer(gameId, false, false)
  };

  // Error and loading handlings.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <div className="game-page">
        <div id="text" className="bottom">
          <div className="game-name" title={gameName}>
            {gameName}
          </div>
          <h3>Description</h3>
          <p>{description}</p>
          <h3>Rules</h3>
          <p>
            The game begins with one or more “Original Zombies” (OZ) or patient
            zero. The purpose of the OZ is to infect human players by tagging
            them. Once tagged, a human becomes a zombie for the remainder of the
            game. Human players are able to defend themselves against the zombie
            horde using Nerf weapons and clean, rolled-up socks which may be
            thrown to stun an unsuspecting zombie. Many variants of the game
            rules exist which introduce additional rules and activities into the
            game; for example, the Rhodes University variant introduces a
            web-based game map where markers appear showing the location of
            missions and supplies. The appearance of supplies forces humans to
            leave their safe zones and risk being turned.
          </p>
          <h5>Play the Game</h5>
          <Link
            to="/Player"
            onClick={chooseHuman}
            className="btn btn-warning btn-rounded mt-3"
          >
            Join as Human
          </Link>
          <Link
            to="/Player"
            onClick={chooseZombie}
            className="btn btn-warning btn-rounded mx-4 mt-3"
          >
            Join as Zombie
          </Link>
        </div>
      </div>
    );
  }
}
