import React, { useState, useEffect } from "react";
import "./GamePageStyle.css";
import Map from "./Map";
import Chat from "./Chat";
import BiteCode from "./BiteCode";
import { getGame } from "../../api/game";
import { getCurrentPlayer } from "../../api/player";
import { getUser } from "../../api/user";

export default function Player() {
  // Sets error, loading, game, player and user states.
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isGameLoaded, setIsGameLoaded] = useState(false);
  const [isUserLoaded, setIsUserLoaded] = useState(false);
  const [game, setGame] = useState([]);
  const [player, setPlayer] = useState([]);
  const [user, setUser] = useState([]);

  // Gets gameId from localStorage.
  const gameId = localStorage.getItem("gameId");

  const humanOrZombie = () => {
    if (player.human) return "Human";
    else return "Zombie";
  };

  // Get request from API for games.
  const fetchGame = async () => {
    const [error, result] = await getGame(gameId);
    setIsGameLoaded(true);
    setGame(result);
  };

  useEffect(() => {
    setIsGameLoaded(false);
    fetchGame();
  }, [gameId]);

  // Get request from API for players.
  const fetchPlayer = async () => {
    const [error, result] = await getCurrentPlayer(gameId);
    setIsLoaded(true);
    setPlayer(result);
    console.log(result);
  };

  useEffect(() => {
    setIsLoaded(false);
    fetchPlayer();
  }, []);

  // Get request from API for user.
  const fetchUser = async () => {
    const [error, result] = await getUser();
    setIsUserLoaded(true);
    setUser(result);
  };

  useEffect(() => {
    setIsUserLoaded(false);
    fetchUser();
  }, []);

  // Error and loading handlings.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded || !isGameLoaded || !isUserLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <div className="player">
        <Chat />
        <div className="player-text">
          <h2>Welcome {user.firstName}!</h2>
          <h4>
            You are now playing {game.gameName} as a {humanOrZombie()}
          </h4>
        </div>
        {/* For human */}
        {player.human && (
          <p id="text-bite-code">
            <b>Bite Code:</b> {player.biteCode}
          </p>
        )}

        {/* For zombie */}
        {!player.human && <BiteCode />}
        <Map gameId={gameId}></Map>
      </div>
    );
  }
}
