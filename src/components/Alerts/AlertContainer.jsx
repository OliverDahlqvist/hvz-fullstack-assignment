import React, { useState } from 'react';
import { ToastContainer } from 'react-bootstrap';

function AlertContainer({children}) {
    const [show, setShow] = useState(true);

    return (
        <ToastContainer style={{margin: "10px"}} position='top-end'>
            {children}
        </ToastContainer>
    );
}

export default AlertContainer;