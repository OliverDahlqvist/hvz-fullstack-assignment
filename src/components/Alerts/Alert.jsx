import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Toast from 'react-bootstrap/Toast';
import "animate.css";
import "./Alert.css"

function Alert({ text }) {
    const [show, setShow] = useState(true);

    /*const start = () => { setTimeout(() => { setShow(false) }, 5000) }
    start();*/

    if (show) {
        return (
            // <div className='shadow bg-white animate__animated animate__bounce' style={{background: "white", margin: "10px", padding: "1rem", borderRadius: "2rem", width: "200px", height: "55px"}}>
            //     {text}
            // </div>
            <>

                <Toast onClose={() => setShow(false)} show={show} delay={5000} autohide>
                    {/* <Toast.Header>
                        <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"> <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" /> <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                        </svg>
                        <p className="me-auto">{text}</p>
                    </Toast.Header> */}
                    <Toast.Body>{text}</Toast.Body>
                </Toast>
            </>
        )
    }
    else return null;
}

export default Alert;