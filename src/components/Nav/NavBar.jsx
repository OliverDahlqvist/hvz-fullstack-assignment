import React from "react";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Player from "../GamePageComponents/Player";
import Admin from "../AdministrationPageComponents/Admin";
import StartPage from "../LandingPageComponents/FirstPage/StartPage";
import GameOptionList from "../GameOption/GameOptionList";
import "../AdministrationPageComponents/Admin.css";
import "./Navbar.css";
import keycloak from "../../keycloak";
import { ROLES } from "../../consts/roles";
import RenderOnRole from "../../routes/RenderOnRole";
import { registerUser } from "../../api/user";
import { getGames } from "../../api/game";
import { newTest } from "../../api/player";

export default function NavBar() {
  const getLoggedIn = () => {
    if (!!keycloak.token) {
      return (
        <>
          <Nav.Link className="color-nav hover-underline-animation" onClick={keycloak.logout}>Logout</Nav.Link>
          <Nav.Link className="color-nav hover-underline-animation" onClick={() => registerUser()}>Register</Nav.Link>
        </>
      )
    }
    else {
      return (<Nav.Link className="color-nav hover-underline-animation" onClick={keycloak.login}>Login</Nav.Link>)
    }
  }

  return (
    <BrowserRouter>
      <div className="center-tabs" style={{ background: "#f8f9fa" }}>
        <Navbar bg="light" variant="light">
          <Container>
            <Navbar.Brand as={Link} to="/">
              HvZ
            </Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link className="color-nav hover-underline-animation" as={Link} to="/GameOptionList">
                Games
              </Nav.Link>
              <RenderOnRole role={ROLES.Admin}>
                <Nav.Link className="color-nav hover-underline-animation" as={Link} to="/Administration">
                  Administration
                </Nav.Link>
              </RenderOnRole>
              {getLoggedIn()}
            </Nav>
          </Container>
        </Navbar>
      </div>
      <Routes>
        <Route path="/" element={<StartPage />} />
        <Route path="/Administration" element={<Admin />} />
        <Route path="/Player" element={<Player />} />
        <Route path="/GameOptionList" element={<GameOptionList />} />
      </Routes>
    </BrowserRouter>
  );
}
