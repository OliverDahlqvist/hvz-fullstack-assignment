import React from "react";
import "./StartPage.css";
import { useState } from "react";
import { useEffect } from "react";
import TrackVisibility from "react-on-screen";

import { Col, Container, Row } from "react-bootstrap";
import adobe2 from "../../../images/adobe2.png";
import { Link } from "react-router-dom";
export const StartPage = () => {
  const responsive = {
    Desktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 4,
    },

    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const [loopNum, setLoopNum] = useState(0);
  const [isDeleting, setIsDeleting] = useState(false);
  const [text, setText] = useState("");
  const [delta, setDelta] = useState(300 - Math.random() * 100);
  const [index, setIndex] = useState(1);
  const toRotate = ["Human vs Zombies", "Human vs Zombies", "Human vs Zombies"];
  const period = 2000;

  useEffect(() => {
    let ticker = setInterval(() => {
      tick();
    }, delta);

    return () => {
      clearInterval(ticker);
    };
  }, [text]);

  const tick = () => {
    let i = loopNum % toRotate.length;
    let fullText = toRotate[i];
    let updatedText = isDeleting
      ? fullText.substring(0, text.length - 1)
      : fullText.substring(0, text.length + 1);

    setText(updatedText);

    if (isDeleting) {
      setDelta((prevDelta) => prevDelta / 2);
    }

    if (!isDeleting && updatedText === fullText) {
      setIsDeleting(true);
      setIndex((prevIndex) => prevIndex - 1);
      setDelta(period);
    } else if (isDeleting && updatedText === "") {
      setIsDeleting(false);
      setLoopNum(loopNum + 1);
      setIndex(1);
      setDelta(500);
    } else {
      setIndex((prevIndex) => prevIndex + 1);
    }
  };

  return (
    <section className="banner" id="home">
      <Container>
        <Row className="aligh-items-center">
          <Col xs={12} md={6} xl={7}>
            <TrackVisibility>
              {({ isVisible }) => (
                <div
                  className={
                    isVisible ? "animate__animated animate__fadeIn" : ""
                  }
                >
                  <h1>
                    {" "}
                    <span
                      className="txt-rotate "
                      dataperiod="1000"
                      data-rotate='[ "Humans vs Zombies" ]'
                    >
                      <span className="wrap">{text}</span>
                    </span>
                  </h1>
                  <p>
                    Humans vs. Zombies (HvZ) is a game of tag played at schools,
                    camps, neighborhoods, libraries, and conventions around the
                    world. The game simulates the exponential spread of a
                    fictional zombie infection through a population.
                  </p>
                  <div className="item">
                  <p>Discover Humans vs Zombies game collection.</p>
                <Link
                
                   to="/GameOptionList"
                  className="btn btn-warning btn-rounded"
             
                >
                Game List
                </Link>
                </div>
                  

                </div>
              )}
            </TrackVisibility>
          </Col>
          <Col xs={12} md={6} xl={5}>
            <TrackVisibility>
              {({ isVisible }) => (
                <div
                  className={
                    isVisible ? "animate__animated animate__zoomIn" : ""
                  }
                >
                  <img src={adobe2} alt="adobe" />
                </div>
              )}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>

    </section>
  );
};

export default StartPage;
