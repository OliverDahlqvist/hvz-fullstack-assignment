import { useState, useEffect } from "react";
import GamePage from "../GamePageComponents/GamePage";
import image from "../../images/land.jpg";
import Table from "react-bootstrap/Table";
import { getGames } from "../../api/game";
import { convertFromId } from "../../util/GameStateConverter";

export default function GameOptionList(props) {
  const [isShown, setIsShown] = useState(false);

  // Sets error, loading and games states.
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [games, setGames] = useState([]);

  // Get request from API.
  const fetchGames = async () => {
    const [error, result] = await getGames();
    setIsLoaded(true);
    setGames(result);
  };

  useEffect(() => {
    setIsLoaded(false);
    fetchGames();
  }, []);

  function handlePreview(gameId, gameName, description) {
    // Get request from API.
    fetchGames();

    localStorage.setItem("gameId", gameId);
    localStorage.setItem("gameName", gameName);
    localStorage.setItem("description", description);
    console.log("gameid:", gameId);
    console.log("gameName", gameName);
    console.log("description", description);
    setIsShown(true);
  }

// Error and loading handlings.
   if (error) {
     return <div>Error: {error.message}</div>;
   } else if (!isLoaded) {
     return <div>Loading...</div>;
   } else {
  return (
    <section
      className="game-list-container"
      style={{ backgroundImage: `url(${image})` }}
    >
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="game-list-bx wow zoomIn">
              <h2>Games</h2>
              <ul></ul>
              <Table striped bordered hover variant="dark">
                <thead>
                  <tr>
                    <th>Game Name</th>
                    <th>Amount of players</th>
                    <th>Game State</th>
                  </tr>
                </thead>
                {games.map((game) => (
                  <>
                    <tbody>
                      <tr>
                        <td>
                          <button
                            id="game-list-button"
                            onClick={() =>
                              handlePreview(
                                game.id,
                                game.gameName,
                                game.description
                              )
                            }
                          >
                            {game.gameName}
                          </button>
                        </td>
                        <td>{game.players.length}</td>
                        <td>{convertFromId(game.gameState)}</td>
                      </tr>
                    </tbody>
                  </>
                ))}
              </Table>
            </div>
            {isShown && <GamePage />}
          </div>
        </div>
      </div>
    </section>
  );
}
}
