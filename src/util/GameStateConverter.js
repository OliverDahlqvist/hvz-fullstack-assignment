export const convertToId = (gameState) => {
    let gameStateValue;
    switch (gameState) {
        case 'Registration':
            gameStateValue = 0;
            break;
        case 'In progress':
            gameStateValue = 1;
            break;
        case 'Complete':
            gameStateValue = 2;
            break;
        default:
            gameStateValue = 0;
            break;
    }
    return gameStateValue;
}

export const convertFromId = (id) => {
    let gameStateValue;
    switch (id) {
        case 0:
            gameStateValue = 'Registration';
            break;
        case 1:
            gameStateValue = 'In progress';
            break;
        case 2:
            gameStateValue = 'Complete';
            break;
        default:
            gameStateValue = 'Error';
            break;
    }
    return gameStateValue;
}