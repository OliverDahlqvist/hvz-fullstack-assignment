import keycloak from "../keycloak";

export const getHeaders = () => {
    if(!!keycloak.token)
    return {headers: { Authorization: `Bearer ${keycloak.token}`, 'Content-Type': 'application/json' }};
    else{
        return {headers: null};
    }
}