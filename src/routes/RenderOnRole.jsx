import keycloak from "../keycloak"

export default function RenderOnRole({ role, children }){
    if(!keycloak.hasRealmRole(role))
        return null;

    return children;
}

export function RenderNotOnRole({ role, children }){
    if(keycloak.hasRealmRole(role))
        return children;

    return null;
}